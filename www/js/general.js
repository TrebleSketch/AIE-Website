/* Source:https://www.w3schools.com/boootstrap/tryit.asp?filename=trybs_ref_js_scrollspy_animate&stacked=h */
$(document).ready(function(){
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".navbar", offset: 50});

  // Thanks Biepbot (@biepbot)
  $(window).on("activate.bs.scrollspy", function(){
    var linkName = $(".navbar-nav .nav-link.active").text();
    history.pushState({}, linkName, '#' + linkName);
  })

  // Add smooth scrolling on all links inside the navbar
  $("#myNavbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
        }, 800);
      } // End if
  });
});