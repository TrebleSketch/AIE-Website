<html lang="en-AU">
<head>
	<?php include "head.html"; ?>

	<link rel="stylesheet" type="text/css" href="css/index.css" />
	
	<script src="js/UnityProgress.js"></script>
	<script src="Build/UnityLoader.js"></script>

	<script>
      var gameInstance = UnityLoader.instantiate("gameContainer", "Build/v0.3.2.json", {onProgress: UnityProgress});
    </script>

	<title>Home | Hidden in Sight</title>
</head>

<body>
	<?php include "banner.html"; ?>

	<?php include "nav.html"; ?>

	<?php include "content-start.html"; ?>

	<!-- Content past here -->
	
	<div class="container-fluid actualcontent" id="description">
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8">
				<h3>Descritpion</h3>
				<p>Lorem ipsum dolor..</p>
				<p>Ut enim ad..</p>
				<p>Osdlk sakd sada sdasw wad.</p>
			</div>
			<div class="col-2"></div>
		</div>
	</div>

	<div class="container-fluid areabreakup">
		<div class="row">
			<div class="col-12">
			</div>
		</div>
	</div>

	<div class="container-fluid actualcontent" id="story">
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8">
				<h3>Story</h3>
				<p>test test est est est est est este st</p>
				<p>test test est est est est est este st</p>
				<p>test test est est est est est este st</p>
				<p>test test est est est est est este st</p>
				<p>test test est est est est est este st</p>
			</div>
			<div class="col-2"></div>
		</div>
	</div>

	<div class="container-fluid areabreakup">
		<div class="row">
			<div class="col-12">
			</div>
		</div>
	</div>

	<div class="container-fluid actualcontent" id="game">
		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<h3>Game</h3>
				<p>Play the game below or download it <a href="./download.php" title="AIE-Platformer Download" alt="AIE-Platformer Download">here</a>!</p>
				<div class="webgl-content">
      				<div id="gameContainer" style="width: 80vw; height: 75vh"></div>
      					<div class="footer-webgl">
        					<div class="webgl-logo"></div>
        					<div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
        					<div class="title">AIE-Platformer</div>
      					</div>
    				</div>
				</div>
				<br/>
			<div class="col-1"></div>
		</div>
	</div>
	
	<?php include "footer.html"; ?>

	<?php include "content-end.html"; ?>
	<script src="js/index.js"></script>
</body>
</html>